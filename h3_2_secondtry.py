#%%
##############################################################################################################
# copy paste below for maintenance

from ibapi.client import EClient
from ibapi.wrapper import EWrapper
from ibapi.contract import Contract
from ibapi.order import Order
import pandas as pd
import threading
import time
import openpyxl


class TradeApp(EWrapper, EClient):
    def __init__(self): 
        EClient.__init__(self, self) 
        self.data = {}
        self.pos_df = pd.DataFrame(columns=['Account', 'Symbol', 'SecType',
                                    'Currency', 'Position', 'Avg cost'])
        self.order_df = pd.DataFrame(columns=['PermId', 'ClientId', 'OrderId',
                                          'Account', 'Symbol', 'SecType',
                                          'Exchange', 'Action', 'OrderType',
                                          'TotalQty', 'CashQty', 'LmtPrice',
                                          'AuxPrice', 'Status'])
         
    def historicalData(self, reqId, bar):
        if reqId not in self.data:
            self.data[reqId] = [{"Date":bar.date,"Open":bar.open,"High":bar.high,"Low":bar.low,"Close":bar.close,"Volume":bar.volume}]
        else:
            self.data[reqId].append({"Date":bar.date,"Open":bar.open,"High":bar.high,"Low":bar.low,"Close":bar.close,"Volume":bar.volume})
        print("reqID:{}, date:{}, open:{}, high:{}, low:{}, close:{}, volume:{}".format(reqId,bar.date,bar.open,bar.high,bar.low,bar.close,bar.volume))
        
    def nextValidId(self, orderId):
        super().nextValidId(orderId)
        self.nextValidOrderId = orderId
        print("NextValidId:", orderId)
        
    def position(self, account, contract, position, avgCost):
        super().position(account, contract, position, avgCost)
        dictionary = {"Account":account, "Symbol": contract.symbol, "SecType": contract.secType,
                      "Currency": contract.currency, "Position": position, "Avg cost": avgCost}
        self.pos_df = self.pos_df.append(dictionary, ignore_index=True)
        
    def positionEnd(self):
        print("Latest position data extracted")
        
    def openOrder(self, orderId, contract, order, orderState):
        super().openOrder(orderId, contract, order, orderState)
        dictionary = {"PermId":order.permId, "ClientId": order.clientId, "OrderId": orderId, 
                      "Account": order.account, "Symbol": contract.symbol, "SecType": contract.secType,
                      "Exchange": contract.exchange, "Action": order.action, "OrderType": order.orderType,
                      "TotalQty": order.totalQuantity, "CashQty": order.cashQty, 
                      "LmtPrice": order.lmtPrice, "AuxPrice": order.auxPrice, "Status": orderState.status}
        self.order_df = self.order_df.append(dictionary, ignore_index=True)
        



#creating object of the Contract class - will be used as a parameter for other function calls
def usTechStk(symbol,sec_type,currency,exchange):
    contract = Contract()
    contract.symbol = 'SPY'
    contract.secType = 'STK'
    contract.currency = 'USD'
    contract.exchange = 'ISLAND'
    return contract 

def usFutures(symbol = "MES", exchange = "GLOBEX", currency = "USD", sectype="FUT", expiry = "20210917", multiplier = "5", local = "MESU1", include_expired = "1", conId = "428519979"):
    contract = Contract()
    contract.symbol = symbol
    contract.secType = sectype
    contract.currency = currency
    contract.exchange = exchange
    contract.LastTradeDateOrContractMonth = expiry
    contract.Multiplier = multiplier
    contract.localSymbol = local
    contract.includeExpired = include_expired
    contract.conId = conId
    return contract

#"index":3,"currency":"USD","exchange":"GLOBEX", "sectype":"FUT", "expiry": "20210917", "multiplier": "50","symbol":"ES", "local":"ESU1", "include_expired": "1", "conId": "428520022"

def usFutures_ES(Symbol, SecType, Exchange, Currency, LastTradeDateOrContractMonth, Multiplier):
    contract = Contract()
    contract.Symbol = "DAX"
    contract.SecType = "FUT"
    contract.Exchange = "DTB"
    contract.Currency = "EUR"
    contract.LastTradeDateOrContractMonth = "201609"
    contract.Multiplier = "5"
    return contract

#             contract.Symbol = "DAX";
#             contract.SecType = "FUT";
#             contract.Exchange = "DTB";
#             contract.Currency = "EUR";
#             contract.LastTradeDateOrContractMonth = "201609";
#             contract.Multiplier = "5";
            
def histData(req_num,contract,duration,candle_size):
    app.reqHistoricalData(reqId=req_num, 
                          contract=contract,
                          endDateTime='',
                          durationStr=duration,
                          barSizeSetting=candle_size,
                          whatToShow='ADJUSTED_LAST',
                          useRTH=1,
                          formatDate=1,
                          keepUpToDate=0,
                          chartOptions=[])	 # EClient function to request contract details

    
def marketOrder(direction,quantity):
    order = Order()
    order.action = direction
    order.orderType = "MKT"
    order.totalQuantity = quantity
    order.tif = "IOC"
    return order

def stopOrder(direction,quantity,st_price):
    order = Order()
    order.action = direction
    order.orderType = "STP"
    order.totalQuantity = quantity
    order.auxPrice = st_price
    return order



def websocket_con():
    app.run()

    
app = TradeApp()      
app.connect("127.0.0.1", 7496, clientId=1) #7496 is real money # 7497 is paper?

# starting a separate daemon thread to execute the websocket connection
con_thread = threading.Thread(target=websocket_con, daemon=True)
con_thread.start()
time.sleep(1) # some latency added to ensure that the connection is established
#%%
# maintenance until above
##############################################################################################################

import pandas as pd
import threading
import time
import openpyxl




#%%


tickers_data = {"INTC" : {"index":0,"currency":"USD","exchange":"ISLAND"}}
# def usTechStk(symbol,sec_type,currency,exchange):

for ticker in tickers_data:
    histData(tickers_data[ticker]["index"],
             usTechStk(ticker,"STK",tickers_data[ticker]["currency"],tickers_data[ticker]["exchange"]),
             '1 M', '1 day')
    time.sleep(5)  # some latency added to ensure that the contract details request has been processed

###################storing trade app object in dataframe#######################
def dataDataframe(ticker_data,TradeApp_obj):
    "returns extracted historical data in dataframe format"
    df_data = {}
    for symbol in ticker_data:
        try:
            df_data[symbol] = pd.DataFrame(TradeApp_obj.data[ticker_data[symbol]["index"]])
            df_data[symbol].set_index("Date",inplace=True)
        except:
            print("error encountered for {} data....skipping".format(symbol))
    return df_data

#extract and store historical data in dataframe
historicalData = dataDataframe(tickers_data,app)

   
dict1={}
dict1=historicalData.copy()
#convert1 = pd.DataFrame.from_dict(dict1, orient='index')
convert1 = pd.DataFrame.from_dict(dict1[0][:], orient ='index', columns=['Open', 'High', 'Low', 'Close', 'Volume', 'Ret'])
convert2 = convert1[0]

#convert2.to_csv("INTC_info.xlsx")

print(convert2)

    # %%

# for a1 in d2_dict_excel:
#     d_3 = {}
#     d_3["df_{}".format(a1)] = historicalData["{}".format(a1)].copy()
#     d_3["df_{}".format(a1)]["ret"]=d_3["df_{}".format(a1)]["Close"].pct_change()
#     d_3["df_{}".format(a1)]["max"]=d_3["df_{}".format(a1)]["High"].rolling(min_periods=1, window=10).max()
#     d_3["df_{}".format(a1)]["min"]=d_3["df_{}".format(a1)]["Low"].rolling(min_periods=1, window=10).min()
#     d_3["df_{}".format(a1)]["mean"]=d_3["df_{}".format(a1)]["Close"].rolling(min_periods=1, window=10).mean()
#     new = pd.DataFrame.from_dict(d_3, orient='index')
#     new = pd.DataFrame.from_dict(d_3, orient ='index', columns=['Open', 'High', 'Low', 'Close', 'Volume', 'Ret', 'Max', 'Min', 'Mean'])
#     new.to_excel(f"{a1}_info.xlsx")
    
#     print(d_3)